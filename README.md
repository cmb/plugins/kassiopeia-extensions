Kassiopeia Extensions
==============

[Kassiopeia](https://katrin-experiment.github.io/Kassiopeia/index.html)
is a software package for the purpose of tracking particles in complex
geometries and electromagnetic fields. It was originally developed for
the simulation of the [KATRIN](https://www.katrin.kit.edu/)
experiment, but was designed to be extremely flexible and has broad
applicability to systems with similar problems. It is primarily of
interest for examining the behavior of particles in electrostatic and
magnetostatic systems at relative low energy scales (<100keV). It also
provides a mechanism to incorporate scattering interactions with
dilute gases and offers specialized treatment of electron transport in
silicon based detectors.

This project is a demonstration of constructing extensions to
[SMTK](https://www.computationalmodelbuilder.org/smtk/) to interface
with Kassiopeia.

License
=======

The Kassiopeia Extensions project is distributed under the OSI-approved
BSD 3-clause License. See [License.txt][] for details.

[License.txt]: LICENSE.txt
