//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/kassiopeia/Registrar.h"

#include "smtk/simulation/kassiopeia/operators/AssignVoltage.h"
#include "smtk/simulation/kassiopeia/operators/ComputeChargeDensity.h"
#include "smtk/simulation/kassiopeia/operators/ComputeEFieldFlux.h"
#include "smtk/simulation/kassiopeia/operators/CreateSphere.h"

#include "smtk/operation/groups/CreatorGroup.h"

#include "smtk/session/mesh/Resource.h"

namespace smtk
{
namespace simulation
{
namespace kassiopeia
{

namespace
{
typedef std::tuple<AssignVoltage, ComputeChargeDensity, ComputeEFieldFlux,
                   CreateSphere> OperationList;
}

void Registrar::registerTo(const smtk::operation::Manager::Ptr& operationManager)
{
  // Register operations
  operationManager->registerOperations<OperationList>();

  smtk::operation::CreatorGroup(operationManager)
    .registerOperation<smtk::session::mesh::Resource,
                       smtk::simulation::kassiopeia::CreateSphere>();
}

void Registrar::unregisterFrom(const smtk::operation::Manager::Ptr& operationManager)
{
  operationManager->unregisterOperations<OperationList>();
}
}
}
}
