<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="assign voltage" Label="Face - Assign Voltage" BaseType="operation">

      <AssociationsDef Name="face" NumberOfRequiredValues="1" Extensible="yes">
        <Accepts>
          <Resource Name="smtk::session::mesh::Resource" Filter="face"/>
        </Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <Double Name="voltage" Label="Voltage" NumberOfRequiredValues="1"/>
      </ItemDefinitions>

    </AttDef>

    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(assign voltage)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
