//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/kassiopeia/operators/AssignVoltage.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/IntItem.h"

#include "smtk/simulation/kassiopeia/AssignVoltage_xml.h"
#include "smtk/session/mesh/Resource.h"
#include "smtk/session/mesh/Session.h"

#include "smtk/mesh/core/CellField.h"
#include "smtk/mesh/core/MeshSet.h"
#include "smtk/mesh/core/Resource.h"

#include "smtk/mesh/utility/Metrics.h"

#include "smtk/model/Face.h"

namespace smtk
{
namespace simulation
{
namespace kassiopeia
{

AssignVoltage::Result AssignVoltage::operateInternal()
{
  smtk::attribute::DoubleItem::Ptr voltageItem = this->parameters()->findDouble("voltage");
  double voltage = voltageItem->value();

  auto associations = this->parameters()->associations();
  auto faces = associations->as<smtk::model::Faces>([](smtk::resource::PersistentObjectPtr obj) {
      return smtk::model::Face(std::dynamic_pointer_cast<smtk::model::Entity>(obj));
  });

  Result result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);

  for (auto& face : faces)
  {
    smtk::mesh::MeshSet faceMesh = face.meshTessellation();
    smtk::mesh::ResourcePtr resource = faceMesh.resource();

    auto voltageField = resource->meshes().cellField("voltage");
    if (!voltageField.isValid())
    {
      std::vector<double> initialVoltages(resource->meshes().cells().size(), 0.);
      resource->meshes().createCellField("voltage", 1, smtk::mesh::FieldType::Double,
                                         &initialVoltages[0]);
    }
    if (!voltageField.isValid())
    {
      smtkErrorMacro(this->log(), "The input model's representative mesh has no voltage field.");
      return this->createResult(smtk::operation::Operation::Outcome::FAILED);
    }

    std::vector<double> voltagesForFace(faceMesh.cells().size(), voltage);
    voltageField.set(faceMesh.cells().range(), voltagesForFace);

    smtk::attribute::ComponentItem::Ptr modified = result->findComponent("modified");
    modified->setValue(face.component());
  }

  return result;
}

const char* AssignVoltage::xmlDescription() const
{
  return AssignVoltage_xml;
}
}
}
}
