//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/kassiopeia/operators/CreateSphere.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/simulation/kassiopeia/CreateSphere_xml.h"
#include "smtk/session/mesh/Resource.h"
#include "smtk/session/mesh/Session.h"

#include "smtk/mesh/core/Resource.h"

#include "smtk/mesh/utility/Metrics.h"

#include "KGMesher.hh"
#include "KGRotatedSurface.hh"

namespace smtk
{
namespace simulation
{
namespace kassiopeia
{

using namespace KGeoBag;

CreateSphere::Result CreateSphere::operateInternal()
{
  smtk::attribute::DoubleItem::Ptr originItem = this->parameters()->findDouble("origin");
  KThreeVector origin(originItem->value(0), originItem->value(1), originItem->value(2));
  KThreeVector xAxis(1.,0.,0.);
  KThreeVector yAxis(0.,1.,0.);
  KThreeVector zAxis(0.,0.,1.);

  smtk::attribute::DoubleItem::Ptr radiusItem = this->parameters()->findDouble("radius");
  double radius = radiusItem->value();

  smtk::attribute::DoubleItem::Ptr minimumAreaItem =
    this->parameters()->findDouble("minimum area");
  double minimumArea = minimumAreaItem->value();

  smtk::attribute::DoubleItem::Ptr maximumAspectRatioItem =
    this->parameters()->findDouble("maximum aspect ratio");
  double maximumAspectRatio = maximumAspectRatioItem->value();

  smtk::attribute::IntItem::Ptr scaleItem = this->parameters()->findInt("scale");
  int scale = scaleItem->value();

  KGRotatedObject* hemi1 = new KGRotatedObject(scale*10, 10);
  {
    double p1[2] = { -radius, 0. };
    double p2[2] = { 0., radius };
    hemi1->AddArc(p2, p1, radius, true);
  }

  KGRotatedObject* hemi2 = new KGRotatedObject(scale*10, 10);
  {
    double p1[2] = { 0., radius };
    double p2[2] = { radius, 0. };
    hemi2->AddArc(p1, p2, radius, false);
  }

  KGMesher mesher;

  // Construct shape placement
  KGRotatedSurface* ih1 = new KGRotatedSurface(hemi1);
  KGSurface* hemisphere1 = new KGSurface(ih1);
  hemisphere1->SetName("hemisphere1");
  hemisphere1->MakeExtension<KGMesh>();
  hemisphere1->AcceptNode(&mesher);

  KGRotatedSurface* ih2 = new KGRotatedSurface(hemi2);
  KGSurface* hemisphere2 = new KGSurface(ih2);
  hemisphere2->SetName("hemisphere2");
  hemisphere2->MakeExtension<KGMesh>();
  hemisphere2->AcceptNode(&mesher);

  std::array<KGSurface*,2> surfaces = {hemisphere1, hemisphere2};

  KGeoBag::KThreeVector currentOrigin(0., 0., 0.);
  KGeoBag::KThreeVector currentXAxis(1., 0., 0.);
  KGeoBag::KThreeVector currentYAxis(0., 1., 0.);
  KGeoBag::KThreeVector currentZAxis(0., 0., 1.);

  auto localToInternal = [&](const KGeoBag::KThreeVector& aVector) -> std::array<double, 3>
    {
      KThreeVector globalVector( currentOrigin +
                                 aVector.X() * currentXAxis +
                                 aVector.Y() * currentYAxis +
                                 aVector.Z() * currentZAxis );
      // KThreeVector internalVector((globalVector - origin).Dot(xAxis),
      //                             (globalVector - origin).Dot(yAxis),
      //                             (globalVector - origin).Dot(zAxis));
      KThreeVector internalVector((origin - globalVector).Dot(xAxis),
                                  (origin - globalVector).Dot(yAxis),
                                  (origin - globalVector).Dot(zAxis));
      return {internalVector.X(), internalVector.Y(), internalVector.Z()};
    };

  int connectivity[4];

  // There are three possible import modes
  //
  // 1. Import a mesh into an existing resource
  // 2. Import a mesh as a new model, but using the session of an existing resource
  // 3. Import a mesh into a new resource

  smtk::session::mesh::Resource::Ptr resource = nullptr;
  smtk::session::mesh::Session::Ptr session = nullptr;
  smtk::mesh::Resource::Ptr meshResource = nullptr;

  // Modes 2 and 3 requre an existing resource for input
  smtk::attribute::ResourceItem::Ptr existingResourceItem =
    this->parameters()->findResource("resource");

  bool newResource = true;
  if (existingResourceItem && existingResourceItem->isEnabled())
  {
    smtk::session::mesh::Resource::Ptr existingResource =
      std::static_pointer_cast<smtk::session::mesh::Resource>(existingResourceItem->value());

    session = existingResource->session();

    smtk::attribute::StringItem::Ptr sessionOnlyItem =
      this->parameters()->findString("session only");
    if (sessionOnlyItem->value() == "import into this file")
    {
      // If the "session only" value is set to "this file", then we use the
      // existing resource
      newResource = false;
      resource = existingResource;
      meshResource = existingResource->resource();
    }
    else
    {
      // If the "session only" value is set to "this session", then we create a
      // new resource with the session from the exisiting resource
      resource = smtk::session::mesh::Resource::create();
      meshResource = smtk::mesh::Resource::create();
      resource->setSession(session);
    }
  }
  else
  {
    // If no existing resource is provided, then we create a new session and
    // resource.
    resource = smtk::session::mesh::Resource::create();
    session = smtk::session::mesh::Session::create();
    meshResource = smtk::mesh::Resource::create();

    // Create a new resource for the import
    resource->setSession(session);
  }

  smtk::mesh::MeshSet preexistingMeshes = meshResource->meshes();

  smtk::mesh::BufferedCellAllocatorPtr allocator =
    meshResource->interface()->bufferedCellAllocator();

  KGMeshElement* meshElement;
  KGMeshTriangle* meshTriangle;
  KGMeshRectangle* meshRectangle;
  KGMeshWire* meshWire;

  double totalArea = 0.;
  int counter = 0;

  std::size_t nElements = 0;

  for (auto surface : surfaces)
  {
    nElements += surface->AsExtension<KGMesh>()->Elements()->size();
  }
  allocator->reserveNumberOfCoordinates(4 * nElements);

  for (auto surface : surfaces)
  {
    KGExtendedSurface<KGMesh>* meshExt = surface->AsExtension<KGMesh>();
    for( vector< KGMeshElement* >::iterator elementIt = meshExt->Elements()->begin();
         elementIt != meshExt->Elements()->end(); elementIt++ )
    {
      meshElement = *elementIt;

      meshTriangle = dynamic_cast< KGMeshTriangle* >( meshElement );
      if( (meshTriangle != NULL) &&
          (meshTriangle->Area() > minimumArea) &&
          (meshTriangle->Aspect() < maximumAspectRatio) )
      {
        totalArea += meshTriangle->Area();

        auto tmp = localToInternal(meshTriangle->GetP0());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[0] = counter++;
        tmp = localToInternal(meshTriangle->GetP1());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[1] = counter++;
        tmp = localToInternal(meshTriangle->GetP2());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[2] = counter++;
        allocator->addCell(smtk::mesh::Triangle, &connectivity[0]);
        continue;
      }

      meshRectangle = dynamic_cast< KGMeshRectangle* >( meshElement );
      if( (meshRectangle != NULL) &&
          (meshRectangle->Area() > minimumArea) &&
          (meshRectangle->Aspect() < maximumAspectRatio) )
      {
        totalArea += meshRectangle->Area();

        auto tmp = localToInternal(meshRectangle->GetP0());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[0] = counter++;
        tmp = localToInternal(meshRectangle->GetP1());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[1] = counter++;
        tmp = localToInternal(meshRectangle->GetP2());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[2] = counter++;
        tmp = localToInternal(meshRectangle->GetP3());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[3] = counter++;
        allocator->addCell(smtk::mesh::Quad, &connectivity[0]);
        continue;
      }

      meshWire = dynamic_cast< KGMeshWire* >( meshElement );
      if( (meshWire != NULL) &&
          (meshWire->Area() > minimumArea) &&
          (meshWire->Aspect() < maximumAspectRatio))
      {
        totalArea += meshTriangle->Area();

        auto tmp = localToInternal(meshWire->GetP0());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[0] = counter++;
        tmp = localToInternal(meshWire->GetP1());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[1] = counter++;
        allocator->addCell(smtk::mesh::Line, &connectivity[0]);
        continue;
      }
    }
  }

  allocator->flush();
  meshResource->createMesh(
    smtk::mesh::CellSet(meshResource, allocator->cells() - preexistingMeshes.cells().range()));
  smtk::mesh::MeshSet allMeshes = meshResource->meshes();
  smtk::mesh::MeshSet newMeshes = smtk::mesh::set_difference(allMeshes, preexistingMeshes);

  // Merge all contact points
  // meshResource->meshes().mergeCoincidentContactPoints();

  // Assign its model manager to the one associated with this session
  meshResource->setModelResource(resource);

  // Also assign the mesh resource to be the model's tessellation
  resource->setMeshTessellations(meshResource);

  // If we are reading a mesh session resource (as opposed to a new import), we
  // should access the existing model instead of creating a new one here. If
  // this is the case, then the mesh resource's associated model id will be related
  // to a model entity that is already in the resource (as it was put there by
  // the Read operation calling this one).
  smtk::common::UUID associatedModelId = meshResource->associatedModel();

  // By default, a model is invalid
  smtk::model::Model model;
  if (associatedModelId != smtk::common::UUID::null() && newResource == true)
  {
    // Assign the model to one described already in the resource with the id of
    // mesh resource's associated model. If there is no such model, then this
    // instance will also be invalid.
    model = smtk::model::Model(resource, associatedModelId);
  }

  if (model.isValid() == false)
  {
    // Determine the model's dimension
    int dimension = int(smtk::mesh::utility::highestDimension(meshResource->meshes()));

    // Create a model with the appropriate dimension
    model = resource->addModel(dimension, dimension);

    // Name the model
    model.setName("sphere");
  }

  // Construct the topology.
  session->addTopology(
    smtk::session::mesh::Topology(model.entity(), newMeshes, false));

  // Declare the model as "dangling" so it will be transcribed.
  session->declareDanglingEntity(model);

  meshResource->associateToModel(model.entity());

  // Set the model's session to point to the current session.
  model.setSession(smtk::model::SessionRef(resource, resource->session()->sessionId()));

  // If we don't call "transcribe" ourselves, it never gets called.
  resource->session()->transcribe(model, smtk::model::SESSION_EVERYTHING, false);

  Result result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);

  {
    smtk::attribute::ResourceItem::Ptr created = result->findResource("resource");
    created->setValue(resource);
  }

  {
    smtk::attribute::ComponentItem::Ptr created = result->findComponent("created");
    created->appendValue(model.component());
  }

  return result;
}

const char* CreateSphere::xmlDescription() const
{
  return CreateSphere_xml;
}

}
}
}
