//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_simulation_kassiopeia_AssignVoltage_h
#define __smtk_simulation_kassiopeia_AssignVoltage_h

#include "smtk/simulation/kassiopeia/Exports.h"

#include "smtk/operation/XMLOperation.h"

namespace smtk
{
namespace simulation
{
namespace kassiopeia
{

class SMTKKASSIOPEIASIMULATION_EXPORT AssignVoltage : public smtk::operation::XMLOperation
{
public:
  smtkTypeMacro(smtk::simulation::kassiopeia::AssignVoltage);
  smtkCreateMacro(AssignVoltage);
  smtkSharedFromThisMacro(smtk::operation::Operation);

protected:
  Result operateInternal() override;
  virtual const char* xmlDescription() const override;
};

}
}
}

#endif
