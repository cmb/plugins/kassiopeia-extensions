<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="compute e field flux" Label="Simulation - Compute E Field Flux" BaseType="operation">

      <AssociationsDef Name="Annotated Resource" NumberOfRequiredValues="1"
                       Extensible="false" OnlyResources="true">
        <Accepts>
          <Resource Name="smtk::session::mesh::Resource"/>
        </Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Double Name="origin" Label="Origin" NumberOfRequiredValues="3">
          <BriefDescription>Sphere origin in Cartesian coordinates</BriefDescription>
          <ComponentLabels>
            <Label>X</Label>
            <Label>Y</Label>
            <Label>Z</Label>
          </ComponentLabels>
          <DefaultValue>0. 0. 0.</DefaultValue>
        </Double>

        <Double Name="radius" Label="Radius" NumberOfRequiredValues="1">
          <BriefDescription>Sphere radius</BriefDescription>
          <DefaultValue>1.</DefaultValue>
        </Double>

        <Int Name="scale" Label="Discretization Scale" NumberOfRequiredValues="1">
          <BriefDescription>Discretization scale</BriefDescription>
          <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
          <DefaultValue>1</DefaultValue>
        </Int>

        <File Name="filename" NumberOfRequiredValues="1"
          ShouldExist="false"
          FileFilters="Exodus II Datasets (*.e *.exo *.ex2);;VTK Polydata (*.vtp);;All files (*.*)">
        </File>
      </ItemDefinitions>

    </AttDef>

    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(compute e field flux)" BaseType="result">
      <ItemDefinitions>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
