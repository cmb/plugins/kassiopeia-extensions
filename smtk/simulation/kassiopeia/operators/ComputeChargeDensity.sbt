<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="compute charge density" Label="Model - Compute Charge Density" BaseType="operation">

      <AssociationsDef Name="Annotated Resource" NumberOfRequiredValues="1"
                       Extensible="false" OnlyResources="true">
        <Accepts>
          <Resource Name="smtk::session::mesh::Resource"/>
        </Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Int Name="solver type" Label="Solver Type">
          <BriefDescription>The type of solver to use</BriefDescription>
          <DiscreteInfo DefaultIndex="2">
            <Value Enum="Gaussian elimination">0</Value>
            <Value Enum="Gauss-Seidel">1</Value>
            <Value Enum="Single-Element Robin Hood (default)">2</Value>
            <Value Enum="Multi-Element Robin Hood">3</Value>
            <Value Enum="BiCGStab">4</Value>
            <Value Enum="BiCGStab w/Jacobi preconditioner">5</Value>
            <Value Enum="Successive Subspace Correction">6</Value>
            <Value Enum="GMRES">7</Value>
          </DiscreteInfo>
        </Int>

        <Double Name="tolerance" Label="Convergence Threshold">
          <BriefDescription>Target relative error of the residual norm</BriefDescription>
          <DefaultValue>1.e-8</DefaultValue>
          <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
        </Double>

      </ItemDefinitions>

    </AttDef>

    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(compute charge density)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
