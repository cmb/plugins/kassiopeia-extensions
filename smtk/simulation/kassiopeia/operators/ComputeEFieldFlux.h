//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_simulation_kassiopeia_ComputeEFieldFlux_h
#define __smtk_simulation_kassiopeia_ComputeEFieldFlux_h

#include "smtk/simulation/kassiopeia/Exports.h"

#include "smtk/operation/XMLOperation.h"

namespace smtk
{
namespace simulation
{
namespace kassiopeia
{

class SMTKKASSIOPEIASIMULATION_EXPORT ComputeEFieldFlux : public smtk::operation::XMLOperation
{
public:
  smtkTypeMacro(smtk::simulation::kassiopeia::ComputeEFieldFlux);
  smtkCreateMacro(ComputeEFieldFlux);
  smtkSharedFromThisMacro(smtk::operation::Operation);

  bool ableToOperate() override;

protected:
  Result operateInternal() override;
  virtual const char* xmlDescription() const override;
};

}
}
}

#endif
