//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/kassiopeia/operators/ComputeEFieldFlux.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/MeshItem.h"
#include "smtk/attribute/ModelEntityItem.h"

#include "smtk/simulation/kassiopeia/ComputeEFieldFlux_xml.h"
#include "smtk/session/mesh/Resource.h"
#include "smtk/session/mesh/Session.h"

#include "smtk/io/ExportMesh.h"

#include "smtk/mesh/core/CellField.h"
#include "smtk/mesh/core/MeshSet.h"
#include "smtk/mesh/core/Resource.h"

#include "smtk/mesh/utility/Metrics.h"

#include "smtk/model/Face.h"

#include "KGMesher.hh"
#include "KGRotatedSurface.hh"

#include "KTransformation.hh"

#include "KTypelist.hh"
#include "KSurfaceTypes.hh"
#include "KSurface.hh"
#include "KSurfaceContainer.hh"

#include "KElectrostaticBoundaryIntegratorFactory.hh"
#include "KBoundaryIntegralMatrix.hh"
#include "KBoundaryIntegralVector.hh"
#include "KBoundaryIntegralSolutionVector.hh"

#include "KBiconjugateGradientStabilized.hh"
#include "KGaussianElimination.hh"
#include "KGaussSeidel.hh"
#include "KGeneralizedMinimalResidual.hh"
#include "KMultiElementRobinHood.hh"
#include "KRobinHood.hh"
#include "KSimpleIterativeKrylovSolver.hh"
#include "KSuccessiveSubspaceCorrection.hh"

#include "KIterativeStateReader.hh"
#include "KIterativeStateWriter.hh"

#include "KElectrostaticIntegratingFieldSolver.hh"

#include <cassert>
#include <limits>

namespace smtk
{
namespace simulation
{
namespace kassiopeia
{
namespace
{

using namespace KEMField;
using namespace KGeoBag;

class AddCellToKEMField : public smtk::mesh::CellForEach
{
public:
  typedef KSurface<KElectrostaticBasis,KDirichletBoundary,KTriangle> KDirichletTriangle;

  AddCellToKEMField(smtk::mesh::CellField& vf, smtk::mesh::CellField& cdf, KSurfaceContainer& sc)
    : smtk::mesh::CellForEach(true)
    , m_voltageField(vf)
    , m_chargeDensityField(cdf)
    , m_surfaceContainer(sc)
  {
  }

  void forCell(const smtk::mesh::Handle& cellId, smtk::mesh::CellType cellType, int) override
  {
    assert(cellType == smtk::mesh::Triangle);
    const double* p = &this->coordinates()[0];

    KDirichletTriangle* t = new KDirichletTriangle();
    t->SetValues(KPosition(p[0], p[1], p[2]),
                 KPosition(p[3], p[4], p[5]),
                 KPosition(p[6], p[7], p[8]));

    smtk::mesh::HandleRange range;
    range.insert(cellId);
    double value = 0.;
    this->m_voltageField.get(range, &value);
    t->SetBoundaryValue(value);
    this->m_chargeDensityField.get(range, &value);
    t->SetSolution(value);

    m_surfaceContainer.push_back(t);
  }

protected:
  smtk::mesh::CellField& m_voltageField;
  smtk::mesh::CellField& m_chargeDensityField;
  KSurfaceContainer& m_surfaceContainer;

};
}

bool ComputeEFieldFlux::ableToOperate()
{
  if (!this->smtk::operation::Operation::ableToOperate())
  {
    return false;
  }

  smtk::attribute::ReferenceItem::Ptr resourceItem = this->parameters()->associations();

  smtk::session::mesh::Resource::Ptr resource =
    resourceItem->valueAs<smtk::session::mesh::Resource>();

  if (resource == nullptr)
  {
    smtkInfoMacro(this->log(), "could not access model resource.");
    return false;
  }

  // Access the underlying mesh resource for the model.
  smtk::mesh::ResourcePtr meshResource = resource->resource();
  if (meshResource == nullptr || !meshResource->isValid())
  {
    smtkInfoMacro(this->log(), "No mesh resource associated with this model.");
    return false;
  }

  auto voltageField = meshResource->meshes().cellField("voltage");
  if (!voltageField.isValid())
  {
    smtkInfoMacro(this->log(), "The input model's representative mesh has no voltage cell field.");
    return false;
  }

  auto chargeDensityField = meshResource->meshes().cellField("charge density");
  if (!chargeDensityField.isValid())
  {
    smtkInfoMacro(this->log(), "The input model's representative mesh has no charge density cell field.");
    return false;
  }

  return true;
 }

ComputeEFieldFlux::Result ComputeEFieldFlux::operateInternal()
{
  smtk::attribute::ReferenceItem::Ptr resourceItem = this->parameters()->associations();

  smtk::session::mesh::Resource::Ptr resource =
    resourceItem->valueAs<smtk::session::mesh::Resource>();

  smtk::mesh::ResourcePtr meshResource = resource->resource();

  auto voltageField = meshResource->meshes().cellField("voltage");
  auto chargeDensityField = meshResource->meshes().cellField("charge density");
  KSurfaceContainer surfaceContainer;

  AddCellToKEMField addCell(voltageField, chargeDensityField, surfaceContainer);
  smtk::mesh::for_each(chargeDensityField.cells(), addCell);

  KElectrostaticBoundaryIntegrator integrator =
    KElectrostaticBoundaryIntegratorFactory::Make("analytic");

  KIntegratingFieldSolver<KElectrostaticBoundaryIntegrator> field(surfaceContainer,integrator);

  smtk::attribute::DoubleItem::Ptr originItem = this->parameters()->findDouble("origin");
  KThreeVector displacement(originItem->value(0), originItem->value(1), originItem->value(2));

  smtk::attribute::DoubleItem::Ptr radiusItem = this->parameters()->findDouble("radius");
  double radius = radiusItem->value();

  smtk::attribute::IntItem::Ptr scaleItem = this->parameters()->findInt("scale");
  int scale = scaleItem->value();

  KGRotatedObject* hemisphere = new KGRotatedObject(scale*10, 10);
  {
    double p1[2] = { 0., radius };
    double p2[2] = { -radius, 0. };
    hemisphere->AddArc(p1, p2, radius, true);
  }

  KThreeVector origin(0., 0., 0.);
  KThreeVector xAxis(1.,0.,0.);
  KThreeVector yAxis(0.,1.,0.);
  KThreeVector zAxis(0.,0.,1.);

  KGMesher mesher;

  // Construct shape placement
  KGRotatedSurface* ih1 = new KGRotatedSurface(hemisphere);

  KTransformation transformation1;
  transformation1.SetDisplacement(displacement);

  KGSurface* hemisphere1 = new KGSurface(ih1);
  hemisphere1->SetName("hemisphere1");
  hemisphere1->Transform(&transformation1);
  hemisphere1->MakeExtension<KGMesh>();
  hemisphere1->AcceptNode(&mesher);

  KTransformation transformation2;
  transformation2.SetDisplacement(displacement);
  transformation2.SetRotatedFrame(xAxis, -1. * yAxis, -1. * zAxis);

  KGSurface* hemisphere2 = new KGSurface(ih1);
  hemisphere2->SetName("hemisphere2");
  hemisphere2->Transform(&transformation2);
  hemisphere2->MakeExtension<KGMesh>();
  hemisphere2->AcceptNode(&mesher);

  std::array<KGSurface*,2> surfaces = {hemisphere1, hemisphere2};

  int connectivity[4];

  smtk::mesh::Resource::Ptr fieldResource = smtk::mesh::Resource::create();
  smtk::mesh::BufferedCellAllocatorPtr allocator =
    fieldResource->interface()->bufferedCellAllocator();

  KGMeshElement* meshElement;
  KGMeshTriangle* meshTriangle;
  KGMeshRectangle* meshRectangle;
  KGMeshWire* meshWire;

  int counter = 0;
  int cellCounter = 0;

  std::size_t nElements = 0;

  for (auto surface : surfaces)
  {
    nElements += surface->AsExtension<KGMesh>()->Elements()->size();
  }
  allocator->reserveNumberOfCoordinates(4 * nElements);

  std::vector<double> area(nElements);
  std::vector<double> potential(nElements);
  std::vector<double> efield(3 * nElements);
  std::vector<double> normal(3 * nElements);
  std::vector<double> position(3 * nElements);

  for (auto surface : surfaces)
  {
    KGeoBag::KThreeVector currentOrigin = surface->GetOrigin();
    KGeoBag::KThreeVector currentXAxis = surface->GetXAxis();
    KGeoBag::KThreeVector currentYAxis = surface->GetYAxis();
    KGeoBag::KThreeVector currentZAxis = surface->GetZAxis();

    auto localToInternal = [&](const KGeoBag::KThreeVector& aVector) -> std::array<double, 3>
      {
        KThreeVector globalVector( currentOrigin +
                                   aVector.X() * currentXAxis +
                                   aVector.Y() * currentYAxis +
                                   aVector.Z() * currentZAxis );
        KThreeVector internalVector((globalVector - origin).Dot(xAxis),
                                    (globalVector - origin).Dot(yAxis),
                                    (globalVector - origin).Dot(zAxis));
        return {internalVector.X(), internalVector.Y(), internalVector.Z()};
      };

    KGExtendedSurface<KGMesh>* meshExt = surface->AsExtension<KGMesh>();
    for (vector< KGMeshElement* >::iterator elementIt = meshExt->Elements()->begin();
         elementIt != meshExt->Elements()->end(); ++elementIt, ++cellCounter )
    {
      meshElement = *elementIt;

      meshTriangle = dynamic_cast< KGMeshTriangle* >( meshElement );
      if (meshTriangle != NULL)
      {
        auto tmp = localToInternal(meshTriangle->GetP0());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[0] = counter++;
        tmp = localToInternal(meshTriangle->GetP1());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[1] = counter++;
        tmp = localToInternal(meshTriangle->GetP2());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[2] = counter++;

        KThreeVector p_ =
          (meshTriangle->GetP0() + meshTriangle->GetP1() + meshTriangle->GetP2())/3.;

        KEMThreeVector p; p.SetComponents(p_[0], p_[1], p_[2]);
        KEMThreeVector f = field.ElectricField(p);
        for (int i = 0; i < 3; i++)
        {
          efield[3*cellCounter + i] = f[i];
        }

        area[cellCounter] = meshTriangle->Area();
        potential[cellCounter] = field.Potential(p);
        allocator->addCell(smtk::mesh::Triangle, &connectivity[0]);
        continue;
      }

      meshRectangle = dynamic_cast< KGMeshRectangle* >( meshElement );
      if(meshRectangle != NULL)
      {
        auto tmp = localToInternal(meshRectangle->GetP0());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[0] = counter++;
        tmp = localToInternal(meshRectangle->GetP1());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[1] = counter++;
        tmp = localToInternal(meshRectangle->GetP2());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[2] = counter++;
        tmp = localToInternal(meshRectangle->GetP3());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[3] = counter++;
        allocator->addCell(smtk::mesh::Quad, &connectivity[0]);
        continue;
      }

      meshWire = dynamic_cast< KGMeshWire* >( meshElement );
      if(meshWire != NULL)
      {
        auto tmp = localToInternal(meshWire->GetP0());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[0] = counter++;
        tmp = localToInternal(meshWire->GetP1());
        allocator->setCoordinate(counter, &tmp[0]);
        connectivity[1] = counter++;
        allocator->addCell(smtk::mesh::Line, &connectivity[0]);
        continue;
      }
    }
  }

  allocator->flush();
  fieldResource->createMesh(smtk::mesh::CellSet(fieldResource, allocator->cells()));

  // Merge all contact points
  fieldResource->meshes().mergeCoincidentContactPoints();

  fieldResource->meshes().createCellField("area", 1, smtk::mesh::FieldType::Double, &area[0]);
  fieldResource->meshes().createCellField("electric field", 3, smtk::mesh::FieldType::Double,
                                          &efield[0]);
  fieldResource->meshes().createCellField("potential", 1, smtk::mesh::FieldType::Double,
                                          &potential[0]);

  smtk::attribute::FileItem::Ptr filePathItem = this->parameters()->findFile("filename");

  std::string filePath = filePathItem->value();

  smtk::io::ExportMesh exportMesh;
  bool exportSuccess = exportMesh(filePath, fieldResource);

  if (!exportSuccess)
  {
    smtkErrorMacro(this->log(), "Field collection failed to export.");
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  return this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
}

const char* ComputeEFieldFlux::xmlDescription() const
{
  return ComputeEFieldFlux_xml;
}

}
}
}
