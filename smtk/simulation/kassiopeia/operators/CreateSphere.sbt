<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="create sphere" Label="Model - Create Sphere" BaseType="operation">

      <BriefDescription>
        Construct a discretized sphere
      </BriefDescription>

      <ItemDefinitions>

        <Double Name="origin" Label="Origin" NumberOfRequiredValues="3">
          <BriefDescription>Sphere origin in Cartesian coordinates</BriefDescription>
          <DefaultValue>0. 0. 0.</DefaultValue>
        </Double>

        <Double Name="radius" Label="Radius" NumberOfRequiredValues="1">
          <BriefDescription>Sphere radius</BriefDescription>
          <DefaultValue>1.</DefaultValue>
        </Double>

        <Double Name="minimum area" Label="Minimum Area" NumberOfRequiredValues="1" AdvanceLevel="1">
          <BriefDescription>Minimum acceptable area for discretized element</BriefDescription>
          <DefaultValue>1.e-10</DefaultValue>
        </Double>

        <Double Name="maximum aspect ratio" Label="Maximum Aspect Ratio" NumberOfRequiredValues="1" AdvanceLevel="1">
          <BriefDescription>Maximum acceptable aspect ratio for discretized element</BriefDescription>
          <DefaultValue>1.e10</DefaultValue>
        </Double>

        <Int Name="scale" Label="Discretization Scale" NumberOfRequiredValues="1">
          <BriefDescription>Discretization scale</BriefDescription>
          <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
          <DefaultValue>1</DefaultValue>
        </Int>

        <Resource Name="resource" Label="Import into" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <Accepts>
            <Resource Name="smtk::session::mesh::Resource"/>
          </Accepts>
        </Resource>

        <String Name="session only" Label="session" AdvanceLevel="1">
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="this file">import into this file</Value>
            </Structure>
            <Structure>
              <Value Enum="this session">import into a new file using this file's session</Value>
            </Structure>
          </DiscreteInfo>
        </String>

      </ItemDefinitions>

    </AttDef>
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(create sphere)" BaseType="result">
      <ItemDefinitions>

        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::session::mesh::Resource"/>
          </Accepts>
        </Resource>

      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <Views>
    <View Type="Operation" Title="Model - Create Uniform Grid"
          FilterByAdvanceLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="createUniformGrid"/>
      </InstancedAttributes>
    </View>
  </Views>

</SMTK_AttributeResource>
