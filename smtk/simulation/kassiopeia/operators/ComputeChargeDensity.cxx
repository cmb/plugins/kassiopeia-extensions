//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/kassiopeia/operators/ComputeChargeDensity.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/IntItem.h"

#include "smtk/simulation/kassiopeia/ComputeChargeDensity_xml.h"
#include "smtk/session/mesh/Resource.h"
#include "smtk/session/mesh/Session.h"

#include "smtk/mesh/core/CellField.h"
#include "smtk/mesh/core/MeshSet.h"
#include "smtk/mesh/core/Resource.h"

#include "smtk/mesh/utility/Metrics.h"

#include "smtk/model/Face.h"

#include "KGMesher.hh"
#include "KGRotatedSurface.hh"

#include "KTypelist.hh"
#include "KSurfaceTypes.hh"
#include "KSurface.hh"
#include "KSurfaceContainer.hh"

#include "KElectrostaticBoundaryIntegratorFactory.hh"
#include "KBoundaryIntegralMatrix.hh"
#include "KBoundaryIntegralVector.hh"
#include "KBoundaryIntegralSolutionVector.hh"

#include "KBiconjugateGradientStabilized.hh"
#include "KGaussianElimination.hh"
#include "KGaussSeidel.hh"
#include "KGeneralizedMinimalResidual.hh"
#include "KMultiElementRobinHood.hh"
#include "KRobinHood.hh"
#include "KSimpleIterativeKrylovSolver.hh"
#include "KSuccessiveSubspaceCorrection.hh"

#ifdef KEMFIELD_USE_OPENCL
#include "KOpenCLSurfaceContainer.hh"
#include "KOpenCLElectrostaticBoundaryIntegratorFactory.hh"
#include "KOpenCLBoundaryIntegralMatrix.hh"
#include "KOpenCLBoundaryIntegralVector.hh"
#include "KOpenCLBoundaryIntegralSolutionVector.hh"
#include "KGaussSeidel_OpenCL.hh"
#include "KRobinHood_OpenCL.hh"
#include "KOpenCLElectrostaticIntegratingFieldSolver.hh"
#ifdef KEMFIELD_USE_MPI
// #include "KGaussSeidel_MPI_OpenCL.hh"
#include "KRobinHood_MPI_OpenCL.hh"
#endif
#endif

#ifdef KEMFIELD_USE_VTK
#include "KEMVTKViewer.hh"
#endif

#include "KIterativeStateReader.hh"
#include "KIterativeStateWriter.hh"

#include <cassert>
#include <limits>

namespace smtk
{
namespace simulation
{
namespace kassiopeia
{
namespace
{

using namespace KEMField;
using namespace KGeoBag;

class AddCellToKEMField : public smtk::mesh::CellForEach
{
public:
  typedef KSurface<KElectrostaticBasis,KDirichletBoundary,KTriangle> KDirichletTriangle;

  AddCellToKEMField(smtk::mesh::CellField& cf, KSurfaceContainer& sc)
    : smtk::mesh::CellForEach(true)
    , m_cellField(cf)
    , m_surfaceContainer(sc)
  {
  }

  void forCell(const smtk::mesh::Handle& cellId, smtk::mesh::CellType cellType, int) override
  {
    assert(cellType == smtk::mesh::Triangle);
    const double* p = &this->coordinates()[0];

    KDirichletTriangle* t = new KDirichletTriangle();
    t->SetValues(KPosition(p[0], p[1], p[2]),
                 KPosition(p[3], p[4], p[5]),
                 KPosition(p[6], p[7], p[8]));

    smtk::mesh::HandleRange range;
    range.insert(cellId);
    double value = 0.;
    this->m_cellField.get(range, &value);
    t->SetBoundaryValue(value);

    m_surfaceContainer.push_back(t);
  }

protected:
  smtk::mesh::CellField& m_cellField;
  KSurfaceContainer& m_surfaceContainer;

};
}

bool ComputeChargeDensity::ableToOperate()
{
  if (!this->smtk::operation::Operation::ableToOperate())
  {
    return false;
  }

  smtk::attribute::ReferenceItem::Ptr resourceItem = this->parameters()->associations();

  smtk::session::mesh::Resource::Ptr resource =
    resourceItem->valueAs<smtk::session::mesh::Resource>();

  if (resource == nullptr)
  {
    smtkInfoMacro(this->log(), "could not access model resource.");
    return false;
  }

  // Access the underlying mesh resource for the model.
  smtk::mesh::ResourcePtr meshResource = resource->resource();
  if (meshResource == nullptr || !meshResource->isValid())
  {
    smtkInfoMacro(this->log(), "No mesh resource associated with this model.");
    return false;
  }

  auto voltageField = meshResource->meshes().cellField("voltage");
  if (!voltageField.isValid())
  {
    smtkInfoMacro(this->log(), "The input model's representative mesh has no voltage cell field.");
    return false;
  }

  return true;
 }

ComputeChargeDensity::Result ComputeChargeDensity::operateInternal()
{
  bool cache = false;
  int rh_increment = 0;
  int rh_dimension = 2;
  bool residualGraph = false;
  int saveIncrement = std::numeric_limits<int>::max();

  smtk::attribute::DoubleItem::Ptr toleranceItem =
    this->parameters()->findDouble("tolerance");
  double tolerance = toleranceItem->value();

  // 0: Gaussian elimination
  // 1: Gauss-Seidel
  // 2: Single-Element Robin Hood (default)
  // 3: Multi-Element Robin Hood
  // 4: BiCGStab
  // 5: BiCGStab w/Jacobi preconditioner
  // 6: Successive Subspace Correction
  // 7: GMRES
  smtk::attribute::IntItem::Ptr solverTypeItem = this->parameters()->findInt("solver type");
  int method = solverTypeItem->value();

  smtk::attribute::ReferenceItem::Ptr resourceItem = this->parameters()->associations();

  smtk::session::mesh::Resource::Ptr resource =
    resourceItem->valueAs<smtk::session::mesh::Resource>();

  smtk::mesh::ResourcePtr meshResource = resource->resource();

  auto voltageField = meshResource->meshes().cellField("voltage");
  KSurfaceContainer surfaceContainer;

  AddCellToKEMField addCell(voltageField, surfaceContainer);
  smtk::mesh::for_each(voltageField.cells(), addCell);

#ifdef KEMFIELD_USE_OPENCL
  KOpenCLSurfaceContainer oclSurfaceContainer(surfaceContainer);
  KOpenCLElectrostaticBoundaryIntegrator integrator{KoclEBIFactory::Make("analytic",oclSurfaceContainer)};
  KSquareMatrix<double>* A = new KBoundaryIntegralMatrix<KOpenCLBoundaryIntegrator<KElectrostaticBasis> >(oclSurfaceContainer,integrator);

  KBoundaryIntegralVector<KOpenCLBoundaryIntegrator<KElectrostaticBasis> > b(oclSurfaceContainer,integrator);
  KBoundaryIntegralSolutionVector<KOpenCLBoundaryIntegrator<KElectrostaticBasis> > x(oclSurfaceContainer,integrator);
#else
  KElectrostaticBoundaryIntegrator integrator =
    KElectrostaticBoundaryIntegratorFactory::Make("analytic");
  KSquareMatrix<double>* A;
  if (cache)
  {
    A = new KBoundaryIntegralMatrix<KElectrostaticBoundaryIntegrator,true>(surfaceContainer,
                                                                           integrator);
  }
  else
  {
    A = new KBoundaryIntegralMatrix<KElectrostaticBoundaryIntegrator>(surfaceContainer, integrator);
  }

  KBoundaryIntegralSolutionVector<KElectrostaticBoundaryIntegrator> x(surfaceContainer, integrator);
  KBoundaryIntegralVector<KElectrostaticBoundaryIntegrator> b(surfaceContainer, integrator);
#endif

  if (method == 0)
  {
    KGaussianElimination<KElectrostaticBoundaryIntegrator::ValueType> gaussianElimination;
    gaussianElimination.Solve(*A,x,b);
  }
  else if (method == 1)
  {
#if defined(KEMFIELD_USE_MPI)
      KGaussSeidel<KElectrostaticBoundaryIntegrator::ValueType,
           KGaussSeidel_MPI> gaussSeidel;
#elif defined(KEMFIELD_USE_OPENCL)
      KGaussSeidel<KElectrostaticBoundaryIntegrator::ValueType,
           KGaussSeidel_OpenCL> gaussSeidel;
#ifndef KEMFIELD_USE_DOUBLE_PRECISION
      gaussSeidel.SetTolerance((tolerance > 1.e-5 ? tolerance : 1.e-5));
#else
      gaussSeidel.SetTolerance(tolerance);
#endif
#else
      KGaussSeidel<KElectrostaticBoundaryIntegrator::ValueType> gaussSeidel;
      gaussSeidel.SetTolerance(tolerance);
#endif

      MPI_SINGLE_PROCESS
      {
    gaussSeidel.AddVisitor(new KIterationDisplay<KElectrostaticBoundaryIntegrator::ValueType>());
      }

// #ifdef KEMFIELD_USE_VTK
//       if (residualGraph)
//     MPI_SINGLE_PROCESS
//       gaussSeidel.AddVisitor(new KVTKResidualGraph<double>());
// #endif

// #ifdef KEMFIELD_USE_VTK
//       if (residualGraph)
//       gaussSeidel.AddVisitor(new KVTKResidualGraph<double>());
// #endif

    KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>* stateReader = new KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    gaussSeidel.AddVisitor(stateReader);

    KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>* stateWriter = new KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    stateWriter->Interval(saveIncrement);
    gaussSeidel.AddVisitor(stateWriter);

    gaussSeidel.Solve(*A,x,b);
  }
  else if (method == 2)
  {
#if defined(KEMFIELD_USE_MPI) && defined(KEMFIELD_USE_OPENCL)
      KRobinHood<KElectrostaticBoundaryIntegrator::ValueType,
         KRobinHood_MPI_OpenCL> robinHood;
#ifndef KEMFIELD_USE_DOUBLE_PRECISION
      robinHood.SetTolerance((tolerance > 1.e-5 ? tolerance : 1.e-5));
#else
      robinHood.SetTolerance(tolerance);
#endif
#elif defined(KEMFIELD_USE_MPI)
      KRobinHood<KElectrostaticBoundaryIntegrator::ValueType,
         KRobinHood_MPI> robinHood;
#elif defined(KEMFIELD_USE_OPENCL)
      KRobinHood<KElectrostaticBoundaryIntegrator::ValueType,
         KRobinHood_OpenCL> robinHood;
#ifndef KEMFIELD_USE_DOUBLE_PRECISION
      robinHood.SetTolerance((tolerance > 1.e-5 ? tolerance : 1.e-5));
#else
      robinHood.SetTolerance(tolerance);
#endif
#else
      KRobinHood<KElectrostaticBoundaryIntegrator::ValueType> robinHood;
      robinHood.SetTolerance(tolerance);
#endif

      MPI_SINGLE_PROCESS
      {
    robinHood.AddVisitor(new KIterationDisplay<KElectrostaticBoundaryIntegrator::ValueType>());
      }

    KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>* stateReader = new KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    robinHood.AddVisitor(stateReader);

    KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>* stateWriter = new KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    stateWriter->Interval(saveIncrement);
    robinHood.AddVisitor(stateWriter);

    robinHood.SetResidualCheckInterval((rh_increment ? rh_increment : b.Dimension()));
// #ifdef KEMFIELD_USE_VTK
//       if (residualGraph)
//       {
//     robinHood.SetResidualCheckInterval(1);
//     MPI_SINGLE_PROCESS
//       robinHood.AddVisitor(new KVTKResidualGraph<double>());
//       }
// #endif

    robinHood.Solve(*A,x,b);
  }
  else if (method == 3)
  {
    KMultiElementRobinHood<KElectrostaticBoundaryIntegrator::ValueType,
                           KMultiElementRobinHood_SingleThread> robinHood;
    robinHood.SetSubspaceDimension(rh_dimension);
    robinHood.SetTolerance(tolerance);

    robinHood.AddVisitor(new KIterationDisplay<KElectrostaticBoundaryIntegrator::ValueType>());

    KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>* stateReader = new KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    robinHood.AddVisitor(stateReader);

    KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>* stateWriter = new KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    stateWriter->Interval(saveIncrement);
    robinHood.AddVisitor(stateWriter);

    robinHood.SetResidualCheckInterval((rh_increment ? rh_increment : b.Dimension()/2));

// #ifdef KEMFIELD_USE_VTK
//       if (residualGraph)
//       {
//     robinHood.SetResidualCheckInterval(1);
//     MPI_SINGLE_PROCESS
//       robinHood.AddVisitor(new KVTKResidualGraph<double>());
//       }
// #endif

      robinHood.Solve(*A,x,b);
  }
  else if (method == 4)
  {
    KSimpleIterativeKrylovSolver<KElectrostaticBoundaryIntegrator::ValueType, KBiconjugateGradientStabilized> biCGStab;
    biCGStab.SetTolerance(tolerance);

    biCGStab.AddVisitor(new KIterationDisplay<KElectrostaticBoundaryIntegrator::ValueType>());

    KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>* stateReader = new KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    biCGStab.AddVisitor(stateReader);

    KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>* stateWriter = new KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    stateWriter->Interval(saveIncrement);
    biCGStab.AddVisitor(stateWriter);

// #ifdef KEMFIELD_USE_VTK
//       if (residualGraph)
//       {
//       biCGStab.AddVisitor(new KVTKResidualGraph<double>());
//       }
// #endif

    biCGStab.Solve(*A,x,b);
  }
  else if (method == 5)
  {
    //TODO, make this preconditioned!!
    KSimpleIterativeKrylovSolver<KElectrostaticBoundaryIntegrator::ValueType, KBiconjugateGradientStabilized> biCGStab;
    biCGStab.SetTolerance(tolerance);

    biCGStab.AddVisitor(new KIterationDisplay<KElectrostaticBoundaryIntegrator::ValueType>());

    KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>* stateReader = new KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    biCGStab.AddVisitor(stateReader);

    KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>* stateWriter = new KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    stateWriter->Interval(saveIncrement);
    biCGStab.AddVisitor(stateWriter);

// #ifdef KEMFIELD_USE_VTK
//       if (residualGraph)
//       {
//     MPI_SINGLE_PROCESS
//       biCGStab.AddVisitor(new KVTKResidualGraph<double>());
//       }
// #endif

    biCGStab.Solve(*A,x,b);
  }
  else if (method == 6)
  {
    KSuccessiveSubspaceCorrection<KElectrostaticBoundaryIntegrator::ValueType,
                                  KSuccessiveSubspaceCorrection_SingleThread> ssc;
    ssc.SetSubspaceDimension(rh_dimension);
    ssc.SetTolerance(tolerance);

    ssc.AddVisitor(new KIterationDisplay<KElectrostaticBoundaryIntegrator::ValueType>());

    KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>* stateReader = new KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    ssc.AddVisitor(stateReader);

    KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>* stateWriter = new KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    stateWriter->Interval(saveIncrement);
    ssc.AddVisitor(stateWriter);

    ssc.SetResidualCheckInterval((rh_increment ? rh_increment : b.Dimension()/2));

// #ifdef KEMFIELD_USE_VTK
//       if (residualGraph)
//       {
//     ssc.SetResidualCheckInterval(1);

//     MPI_SINGLE_PROCESS
//       ssc.AddVisitor(new KVTKResidualGraph<double>());
//       }
// #endif

    ssc.Solve(*A,x,b);
  }
  else if (method == 7)
  {
    KSimpleIterativeKrylovSolver< KElectrostaticBoundaryIntegrator::ValueType, KGeneralizedMinimalResidual> gmres;
    gmres.SetTolerance(tolerance);
    //gmres.SetRestartParameter(30);

    gmres.AddVisitor(new KIterationDisplay<KElectrostaticBoundaryIntegrator::ValueType>());

    KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>* stateReader = new KIterativeStateReader<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    gmres.AddVisitor(stateReader);

    KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>* stateWriter = new KIterativeStateWriter<KElectrostaticBoundaryIntegrator::ValueType>(surfaceContainer);
    stateWriter->Interval(saveIncrement);
    gmres.AddVisitor(stateWriter);

    gmres.Solve(*A,x,b);
  }

  std::vector<double> chargeDensities(x.Dimension());
  std::vector<double> areas(x.Dimension());
  for (std::size_t i = 0; i < x.Dimension(); i++)
  {
    areas[i] = surfaceContainer[i]->GetShape()->Area();
    chargeDensities[i] = x(i);
  }

  delete A;

  meshResource->meshes().createCellField("charge density", 1, smtk::mesh::FieldType::Double,
                                         &chargeDensities[0]);
  meshResource->meshes().createCellField("area", 1, smtk::mesh::FieldType::Double, &areas[0]);

  Result result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
  smtk::attribute::ComponentItem::Ptr modified = result->findComponent("modified");
  smtk::resource::Component::Visitor addModelsToModified =
    [&](const smtk::resource::ComponentPtr& component)
    {
      auto entry = std::dynamic_pointer_cast<smtk::model::Entity>(component);
      if (entry && entry->isModel())
      {
        modified->appendValue(entry);
      }
    };
  resource->visit(addModelsToModified);

  return result;
}

const char* ComputeChargeDensity::xmlDescription() const
{
  return ComputeChargeDensity_xml;
}
}
}
}
