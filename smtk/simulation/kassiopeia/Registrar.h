//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __smtk_simulation_kassiopeia_Registrar_h
#define __smtk_simulation_kassiopeia_Registrar_h

#include "smtk/simulation/kassiopeia/Exports.h"

#include "smtk/session/mesh/Registrar.h"
#include "smtk/operation/Manager.h"
#include "smtk/resource/Manager.h"

namespace smtk
{
namespace simulation
{
namespace kassiopeia
{

class SMTKKASSIOPEIASIMULATION_EXPORT Registrar
{
public:
  using Dependencies = std::tuple<smtk::session::mesh::Registrar>;

  static void registerTo(const smtk::operation::Manager::Ptr&);
  static void unregisterFrom(const smtk::operation::Manager::Ptr&);
};
}
}
}

#endif
