//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/mesh/Resource.h"

#include "smtk/simulation/kassiopeia/Registrar.h"
#include "smtk/simulation/kassiopeia/operators/AssignVoltage.h"
#include "smtk/simulation/kassiopeia/operators/ComputeChargeDensity.h"
#include "smtk/simulation/kassiopeia/operators/ComputeEFieldFlux.h"
#include "smtk/simulation/kassiopeia/operators/CreateSphere.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/VoidItem.h"

#include "smtk/io/ImportMesh.h"

#include "smtk/mesh/testing/cxx/helpers.h"

#include "smtk/mesh/core/CellField.h"
#include "smtk/mesh/core/MeshSet.h"
#include "smtk/mesh/core/Resource.h"

#include "smtk/model/CellEntity.h"
#include "smtk/model/EntityRef.h"
#include "smtk/model/Face.h"
#include "smtk/model/Resource.h"

#include "smtk/operation/Manager.h"
#include "smtk/resource/Manager.h"

//force to use filesystem version 3
#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>
using namespace boost::filesystem;

#include "KEMConstants.hh"

namespace
{
std::string writeRoot = SCRATCH_DIR;

void cleanup(const std::string& file_path)
{
  //first verify the file exists
  ::boost::filesystem::path path(file_path);
  if (::boost::filesystem::is_regular_file(path))
  {
    //remove the file_path if it exists.
    ::boost::filesystem::remove(path);
  }
}

  class SumCharge : public smtk::mesh::CellForEach
  {
  public:
    SumCharge(smtk::mesh::CellField& cf, smtk::mesh::CellField& af)
      : smtk::mesh::CellForEach(false)
      , m_cellField(cf)
      , m_areaField(af)
      , m_totalCharge(0.)
      {
      }

    void forCell(const smtk::mesh::Handle& cellId, smtk::mesh::CellType cellType, int) override
    {
      assert(cellType == smtk::mesh::Triangle);

      smtk::mesh::HandleRange range;
      range.insert(cellId);
      double chargeDensity = 0.;
      this->m_cellField.get(range, &chargeDensity);

      double area = 0.;
      this->m_areaField.get(range, &area);

      m_totalCharge += area * chargeDensity;
    }

    double totalCharge() const { return m_totalCharge; }

  protected:
    smtk::mesh::CellField& m_cellField;
    smtk::mesh::CellField& m_areaField;
    double m_totalCharge;
  };
}

int TwoSpheres(int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  int scale = 5;

  // Create a resource manager
  smtk::resource::Manager::Ptr resourceManager = smtk::resource::Manager::create();

  // Register the mesh resource to the resource manager
  {
    smtk::session::mesh::Registrar::registerTo(resourceManager);
  }

  // Create an operation manager
  smtk::operation::Manager::Ptr operationManager = smtk::operation::Manager::create();

  // Register import operator to the operation manager
  {
    smtk::simulation::kassiopeia::Registrar::registerTo(operationManager);
  }

  // Register the resource manager to the operation manager (newly created
  // resources will be automatically registered to the resource manager).
  operationManager->registerResourceManager(resourceManager);

  // Construct inner sphere
  smtk::session::mesh::Resource::Ptr resource;
  smtk::model::Entity::Ptr innerModel;
  {
    smtk::simulation::kassiopeia::CreateSphere::Ptr createSphereOp =
      operationManager->create<smtk::simulation::kassiopeia::CreateSphere>();
    if (!createSphereOp)
    {
      std::cerr << "No Create Sphere operation\n";
      return 1;
    }

    createSphereOp->parameters()->findDouble("origin")->setValue(0, 0.);
    createSphereOp->parameters()->findDouble("origin")->setValue(1, 0.);
    createSphereOp->parameters()->findDouble("origin")->setValue(2, 0.);
    createSphereOp->parameters()->findDouble("radius")->setValue(1.);
    createSphereOp->parameters()->findInt("scale")->setValue(scale);

    smtk::simulation::kassiopeia::CreateSphere::Result createSphereOpResult =
      createSphereOp->operate();

    smtk::attribute::ComponentItemPtr componentItem =
      std::dynamic_pointer_cast<smtk::attribute::ComponentItem>(
        createSphereOpResult->findComponent("created"));

    innerModel = std::dynamic_pointer_cast<smtk::model::Entity>(componentItem->value());

    if (createSphereOpResult->findInt("outcome")->value() !=
        static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "Create Sphere operation failed\n";
      return 1;
    }

    resource = std::dynamic_pointer_cast<smtk::session::mesh::Resource>(innerModel->resource());
  }

  // Construct outer sphere
  smtk::model::Entity::Ptr outerModel;
  {
    smtk::simulation::kassiopeia::CreateSphere::Ptr createSphereOp =
      operationManager->create<smtk::simulation::kassiopeia::CreateSphere>();
    if (!createSphereOp)
    {
      std::cerr << "No Create Sphere operation\n";
      return 1;
    }

    createSphereOp->parameters()->findResource("resource")->setIsEnabled(true);
    createSphereOp->parameters()->findResource("resource")->setValue(resource);
    createSphereOp->parameters()->findDouble("origin")->setValue(0, 2.5);
    createSphereOp->parameters()->findDouble("origin")->setValue(1, 0.);
    createSphereOp->parameters()->findDouble("origin")->setValue(2, 0.);
    createSphereOp->parameters()->findDouble("radius")->setValue(1.);
    createSphereOp->parameters()->findInt("scale")->setValue(scale);

    smtk::simulation::kassiopeia::CreateSphere::Result createSphereOpResult =
      createSphereOp->operate();

    smtk::attribute::ComponentItemPtr componentItem =
      std::dynamic_pointer_cast<smtk::attribute::ComponentItem>(
        createSphereOpResult->findComponent("created"));

    outerModel = std::dynamic_pointer_cast<smtk::model::Entity>(componentItem->value());

    if (createSphereOpResult->findInt("outcome")->value() !=
        static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "Create Sphere operation failed\n";
      return 1;
    }
  }

  auto assignVoltage = [&](smtk::model::Entity::Ptr& model, double voltage)
    {
      smtk::simulation::kassiopeia::AssignVoltage::Ptr assignVoltageOp =
      operationManager->create<smtk::simulation::kassiopeia::AssignVoltage>();
      if (!assignVoltageOp)
      {
        std::cerr << "No Assign Voltage operation\n";
        return 1;
      }

      smtk::model::Face face = model->referenceAs<smtk::model::Model>().
      cellsAs<std::vector<smtk::model::Face>>()[0];

      if (!face.isValid())
      {
        std::cerr << "Face is invald\n";
        return 1;
      }

      assignVoltageOp->parameters()->associate(face.component());
      assignVoltageOp->parameters()->findDouble("voltage")->setValue(voltage);

      smtk::simulation::kassiopeia::AssignVoltage::Result assignVoltageOpResult =
      assignVoltageOp->operate();

      if (assignVoltageOpResult->findInt("outcome")->value() !=
          static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
      {
        std::cerr << "Assign Voltage operation failed\n";
        return 1;
      }
      return 0;
    };

  if (assignVoltage(innerModel, -1.))
  {
    return 1;
  }
  if (assignVoltage(outerModel, 1.))
  {
    return 1;
  }

  // Compute charge density
  {
    smtk::simulation::kassiopeia::ComputeChargeDensity::Ptr computeChargeDensityOp =
      operationManager->create<smtk::simulation::kassiopeia::ComputeChargeDensity>();
    if (!computeChargeDensityOp)
    {
      std::cerr << "No Compute Charge Density operation\n";
      return 1;
    }

    computeChargeDensityOp->parameters()->associate(resource);

    smtk::simulation::kassiopeia::ComputeChargeDensity::Result computeChargeDensityOpResult =
      computeChargeDensityOp->operate();

    if (computeChargeDensityOpResult->findInt("outcome")->value() !=
        static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "Compute Charge Density operation failed\n";
      return 1;
    }
  }

  // Sum total charge
  double totalCharge = 0.;
  {
    // Access the underlying mesh resource for the model.
    smtk::mesh::ResourcePtr meshResource = resource->resource();

    smtk::mesh::MeshSet mesh = meshResource->meshes();

    std::vector<double> totalChargeForMesh(mesh.size(), 0.);
    {
      smtk::mesh::CellField chargeDensity = mesh.cellField("charge density");
      smtk::mesh::CellField area = mesh.cellField("area");
      for (std::size_t i = 0; i < mesh.size(); i++)
      {
        smtk::mesh::MeshSet submesh = mesh.subset(i);

        SumCharge sumCharge(chargeDensity, area);
        smtk::mesh::for_each(submesh.cells(), sumCharge);
        totalChargeForMesh[i] = sumCharge.totalCharge();
        std::cout<<"Total charge for mesh "<<i<<": "<<totalChargeForMesh[i]<<std::endl;
        totalCharge += totalChargeForMesh[i];
      }
      std::cout<<"Total charge: "<<totalCharge<<std::endl;
      if (fabs(totalCharge)/fabs(totalChargeForMesh[0]) > 5.e-3)
      {
        std::cerr << "Charge fraction ("<<fabs(totalCharge)/fabs(totalChargeForMesh[0])<<") should be 0. (within tolerance).\n";
        return 1;
     }
    }
  }

  return 0;
}
