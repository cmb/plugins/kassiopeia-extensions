//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/mesh/Resource.h"

#include "smtk/simulation/kassiopeia/Registrar.h"
#include "smtk/simulation/kassiopeia/operators/AssignVoltage.h"
#include "smtk/simulation/kassiopeia/operators/ComputeChargeDensity.h"
#include "smtk/simulation/kassiopeia/operators/CreateSphere.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/VoidItem.h"

#include "smtk/mesh/testing/cxx/helpers.h"

#include "smtk/mesh/core/CellField.h"
#include "smtk/mesh/core/MeshSet.h"
#include "smtk/mesh/core/Resource.h"

#include "smtk/model/CellEntity.h"
#include "smtk/model/EntityRef.h"
#include "smtk/model/Face.h"
#include "smtk/model/Resource.h"

#include "smtk/operation/Manager.h"
#include "smtk/resource/Manager.h"

#ifdef SMTK_ENABLE_VTK_SUPPORT
#include "smtk/extension/vtk/source/vtkModelMultiBlockSource.h"

#include "vtkActor.h"
#include "vtkCamera.h"
#include "vtkCommand.h"
#include "vtkCompositePolyDataMapper.h"
#include "vtkDataSetAttributes.h"
#include "vtkInteractorStyleSwitch.h"
#include "vtkNew.h"
#include "vtkPlane.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkProperty.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkSmartPointer.h"
#include "vtkStringArray.h"
#include "vtkXMLMultiBlockDataWriter.h"

// This macro ensures the vtk io library is loaded into the executable
smtkComponentInitMacro(smtk_extension_vtk_io_mesh_MeshIOVTK)
#endif

#include "KEMConstants.hh"

namespace
{
  void VisualizeModel(smtk::model::Model model)
  {
#ifdef SMTK_ENABLE_VTK_SUPPORT
    vtkNew<vtkActor> act;
    vtkNew<vtkModelMultiBlockSource> src;
    vtkNew<vtkCompositePolyDataMapper> map;
    vtkNew<vtkRenderer> ren;
    vtkNew<vtkRenderWindow> win;
    src->SetModelResource(model.resource());
    src->SetDefaultColor(1., 1., 0., 1.);
    map->SetInputConnection(src->GetOutputPort());
    act->SetMapper(map.GetPointer());
    act->GetProperty()->SetPointSize(5);
    act->GetProperty()->SetLineWidth(2);

    vtkNew<vtkCamera> camera;
    camera->SetPosition(-1., -1., -2.);
    camera->SetFocalPoint(0, 0, 0);

    ren->SetActiveCamera(camera.GetPointer());

    win->AddRenderer(ren.GetPointer());
    ren->AddActor(act.GetPointer());

    vtkRenderWindowInteractor* iac = win->MakeRenderWindowInteractor();
    vtkInteractorStyleSwitch::SafeDownCast(iac->GetInteractorStyle())
      ->SetCurrentStyleToTrackballCamera();
    win->SetInteractor(iac);

    win->Render();
    ren->ResetCamera();

    iac->Start();
#else
    (void)model;
#endif
  }
}

int ComputeChargeDensity(int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  // Create a resource manager
  smtk::resource::Manager::Ptr resourceManager = smtk::resource::Manager::create();

  // Register the mesh resource to the resource manager
  {
    smtk::session::mesh::Registrar::registerTo(resourceManager);
  }

  // Create an operation manager
  smtk::operation::Manager::Ptr operationManager = smtk::operation::Manager::create();

  // Register import operator to the operation manager
  {
    smtk::simulation::kassiopeia::Registrar::registerTo(operationManager);
  }

  // Register the resource manager to the operation manager (newly created
  // resources will be automatically registered to the resource manager).
  operationManager->registerResourceManager(resourceManager);

  smtk::model::Entity::Ptr model;

  {
    smtk::simulation::kassiopeia::CreateSphere::Ptr createSphereOp =
      operationManager->create<smtk::simulation::kassiopeia::CreateSphere>();
    if (!createSphereOp)
    {
      std::cerr << "No Create Sphere operation\n";
      return 1;
    }

    createSphereOp->parameters()->findDouble("origin")->setValue(0, 0.);
    createSphereOp->parameters()->findDouble("origin")->setValue(1, 0.);
    createSphereOp->parameters()->findDouble("origin")->setValue(2, 0.);
    createSphereOp->parameters()->findDouble("radius")->setValue(1.);
    createSphereOp->parameters()->findInt("scale")->setValue(3);

    smtk::simulation::kassiopeia::CreateSphere::Result createSphereOpResult =
      createSphereOp->operate();

    smtk::attribute::ComponentItemPtr componentItem =
      std::dynamic_pointer_cast<smtk::attribute::ComponentItem>(
        createSphereOpResult->findComponent("created"));

    model = std::dynamic_pointer_cast<smtk::model::Entity>(componentItem->value());

    if (createSphereOpResult->findInt("outcome")->value() !=
        static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "Create Sphere operation failed\n";
      return 1;
    }
  }

  {
    smtk::simulation::kassiopeia::AssignVoltage::Ptr assignVoltageOp =
      operationManager->create<smtk::simulation::kassiopeia::AssignVoltage>();
    if (!assignVoltageOp)
    {
      std::cerr << "No Assign Voltage operation\n";
      return 1;
    }

    smtk::model::EntityRefs currentEnts =
      std::dynamic_pointer_cast<smtk::model::Resource>(model->resource())
      ->entitiesMatchingFlagsAs<smtk::model::EntityRefs>(smtk::model::FACE);

    // We only extract the first face
    auto eRef = *currentEnts.begin();

    const smtk::model::Face& face = eRef.as<smtk::model::Face>();

    if (!face.isValid())
    {
      std::cerr << "Face is invald\n";
      return 1;
    }

    assignVoltageOp->parameters()->associate(face.component());
    assignVoltageOp->parameters()->findDouble("voltage")->setValue(1.);

    smtk::simulation::kassiopeia::AssignVoltage::Result assignVoltageOpResult =
      assignVoltageOp->operate();

    if (assignVoltageOpResult->findInt("outcome")->value() !=
        static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "Assign Voltage operation failed\n";
      return 1;
    }
  }

  {
    smtk::simulation::kassiopeia::ComputeChargeDensity::Ptr computeChargeDensityOp =
      operationManager->create<smtk::simulation::kassiopeia::ComputeChargeDensity>();
    if (!computeChargeDensityOp)
    {
      std::cerr << "No Compute Charge Density operation\n";
      return 1;
    }

    smtk::session::mesh::Resource::Ptr resource =
      std::dynamic_pointer_cast<smtk::session::mesh::Resource>(model->resource());

    computeChargeDensityOp->parameters()->associate(resource);

    smtk::simulation::kassiopeia::ComputeChargeDensity::Result computeChargeDensityOpResult =
      computeChargeDensityOp->operate();

    if (computeChargeDensityOpResult->findInt("outcome")->value() !=
        static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "Compute Charge Density operation failed\n";
      return 1;
    }
  }

  {
    smtk::session::mesh::Resource::Ptr resource =
      std::dynamic_pointer_cast<smtk::session::mesh::Resource>(model->resource());

    // Access the underlying mesh resource for the model.
    smtk::mesh::ResourcePtr meshResource = resource->resource();

    auto chargeDensityField = meshResource->meshes().cellField("charge density").get<double>();
    auto areaField = meshResource->meshes().cellField("area").get<double>();

    double computedCapacitance = 0;
    for (std::size_t i = 0; i < chargeDensityField.size(); i++)
    {
      computedCapacitance += (chargeDensityField[i] * areaField[i]);
    }
    computedCapacitance /= (4. * KEMField::KEMConstants::Pi * KEMField::KEMConstants::Eps0);

    double expectedCapacitance = 1.;

    std::cout<<"computed capacitance: "<<computedCapacitance<<std::endl;
    std::cout<<"expected capacitance: "<<expectedCapacitance<<std::endl;

    double error = fabs(expectedCapacitance - computedCapacitance)/expectedCapacitance;
    std::cout<<"error: "<<error<<std::endl;

    smtkTest(error < .02, "Spherical capacitance exceeded expected tolerance");
  }

  VisualizeModel(model->referenceAs<smtk::model::Model>());

  return 0;
}
