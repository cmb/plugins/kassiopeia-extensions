#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================

set (unit_tests
  ComputeChargeDensity.cxx
  CreateSphereOp.cxx
  SphericalCapacitor.cxx
  TwoSpheres.cxx
)

set (unit_tests_which_require_data)

set(external_libs ${Boost_LIBRARIES})

if (SMTK_ENABLE_VTK_SUPPORT)
  list (APPEND external_libs ${VTK_LIBRARIES} vtkSMTKSourceExt smtkIOVTK)
endif()

unit_tests(
  LABEL "KassiopeiaSimulation"
  SOURCES ${unit_tests}
  SOURCES_REQUIRE_DATA ${unit_tests_which_require_data}
  LIBRARIES smtkCore smtkKassiopeiaSimulation smtkMeshSession ${external_libs})
